from aiohttp import web
import lxml
import jinja2
import secrets
from markupsafe import Markup

from db import DB

def create_app(*, serve_static = False):
	app = web.Application()

	# Version cache
	app.router.add_get('/AppDirectory/GetAppdirVersion.aspx', handle_getappdirversion)
	# AppDirctory SOAP service
	app.router.add_get('/AppDirectory/AppDirectory.asmx', handle_appdirectory)
	app.router.add_post('/AppDirectory/AppDirectory.asmx', handle_appdirectory)
	# Activities page
	app.router.add_get('/AppDirectory/Directory.aspx', page_directory)
	if serve_static:
		app.router.add_static('/static', 'static')
	
	app.jinja_env = jinja2.Environment(
		loader = jinja2.FileSystemLoader('tmpl'),
		autoescape = jinja2.select_autoescape(default = True),
	)
	app.jinja_env.globals.update({
		'bool_to_str': _bool_to_str,
	})
	
	return app

async def handle_getappdirversion(req):
	return render(req, 'GetAppdirVersion.html', {
		'version': secrets.token_urlsafe(128),
	})

async def handle_appdirectory(req):
	action = None
	ver = None
	
	if req.method == 'POST':
		action = await _preprocess_soap(req)
		if action is None:
			return web.Response(status = 500, text = '')
		action_str = _get_tag_localname(action)
	elif req.method == 'GET':
		action_str = req.query.get('op')
		ver = req.query.get('ver') or ''
		
	if action_str == 'GetFullDataSet':
		results = []
		
		db = DB()
		for apps_locale in db.apps.values():
			results.extend([_create_entry_container(req, i, entry) for i, entry in enumerate(apps_locale)])
		results.extend([_create_category(req, 'en-US', category, i) for i, category in enumerate(db.categories)])
		
		if req.method == 'POST':
			diffgram = _create_diffgram(req, Markup(''.join(results)))
			
			return render(req, 'GetFullDataSetResponse.xml', {
				'diffgram': Markup(diffgram),
			})
		elif req.method == 'GET':
			return render(req, 'AppDir.xml', {
				'v': ver,
				'results': Markup(''.join(results)),
			})
	elif action_str == 'GetFilteredDataSet2':
		locale = _get_action_argument(req, action, 'locale')
		if locale is not None:
			locale = str(locale)
		try:
			page = int(_get_action_argument(req, action, 'Page'))
			kids = int(_get_action_argument(req, action, 'Kids'))
			app_type = int(_get_action_argument(req, action, 'AppType'))
		except:
			return web.Response(status = 500, text = '')
		results = []
		
		db = DB()
		entries = [
			app for app in db.apps['none']
			if app.page == page and app.app_type == app_type
		]
		if locale and locale in db.apps:
			entries.extend([
				app for app in db.apps[locale]
				if app.page == page and app.app_type == app_type
			])
		if kids >= 0:
			entries = [
				app for app in entries
				if app.kids == (kids == 1)
			]
		categories_filtered = []
		for category in db.categories:
			if category.app_type != app_type: continue
			for entry in entries:
				if entry.category_id == category.id:
					categories_filtered.append(category)
					break
		
		results.extend([_create_entry_container(req, i, entry) for i, entry in enumerate(entries)])
		results.extend([_create_category(req, locale, category, i) for i, category in enumerate(categories_filtered)])
		
		if req.method == 'POST':
			diffgram = _create_diffgram(req, Markup(''.join(results)))
			
			return render(req, 'GetFilteredDataSet2Response.xml', {
				'diffgram': Markup(diffgram),
			})
		elif req.method == 'GET':
			return render(req, 'AppDir.xml', {
				'v': ver,
				'results': Markup(''.join(results)),
			})
	elif action_str == 'GetAppEntry':
		try:
			id = int(_get_action_argument(req, action, 'ID'))
		except:
			return web.Response(status = 500, text = '')
		entry_markup = None
		entry = None
		
		db = DB()
		for apps_locale in db.apps.values():
			if entry is not None: break
			
			for app in apps_locale:
				if app.id == id:
					entry = app
					break
		
		if req.method == 'POST':
			if entry:
				entry_markup = Markup(_create_entry(req, 0, entry))
			return render(req, 'GetAppEntryResponse.xml', {
				'entry': entry_markup,
			})
		elif req.method == 'GET':
			if entry:
				return web.Response(status = 200, content_type = 'text/xml', text = _create_entry_container(req, 0, entry, extra = ' code="2" v="{}"'.format(ver)))
			return web.Response(status = 200, content_type = 'text/xml', text = '')
	else:
		return web.Response(status = 500, text = '')

async def page_directory(req):
	app_entries_js = []
	results = []
	locale = req.query.get('L') or 'en-US'
	
	db = DB()
	categories = db.categories
	entries = db.apps
	
	category_tmpl = req.app.jinja_env.get_template('Directory/Directory.category.html')
	entry_tmpl = req.app.jinja_env.get_template('Directory/Directory.entry.html')
	category_end_tmpl = req.app.jinja_env.get_template('Directory/Directory.category.end.html')
	
	for category in categories:
		filtered_entries = [
			app for app in entries['none']
			if app.category_id == category.id
		]
		if locale and locale in entries:
			filtered_entries.extend([
				app for app in entries[locale]
				if app.category_id == category.id
			])
		if not filtered_entries:
			continue
		
		results.append(category_tmpl.render(
			cat_id = category.id,
			cat_name = category.name,
		))
		
		for entry in filtered_entries:
			results.append(entry_tmpl.render(
				app_id = entry.id,
				description = entry.description,
				name = entry.name,
			))
			app_entries_js.append(APP_ENTRY_JS.format(
				entry.id, (1 if entry.kids else 0), entry.min_users, entry.max_users,
			))
		
		results.append(category_end_tmpl.render(
			cat_id = category.id,
		))
	
	return render(req, 'Directory/Directory.html', {
		'app_entries_js': Markup(''.join(app_entries_js)),
		'results': Markup(''.join(results)),
	})

def _get_action_argument(req, action, name):
	result = None
	if req.method == 'POST':
		result = _find_element(action, name)
	elif req.method == 'GET':
		result = req.query.get(name)
	
	return result

def _create_entry_container(req, i, entry, *, extra = None):
	entry_container_tmpl = req.app.jinja_env.get_template('Entry_container.xml')
	if req.method == 'POST':
		extra = Markup(' diffgr:id="Entry{}" msdata:rowOrder="{}" diffgr:hasChanges="inserted"'.format(i + 1, i))
	elif extra is not None and req.method != 'POST':
		extra = Markup(extra)
	
	return entry_container_tmpl.render(
		extra = extra,
		entry = Markup(_create_entry(req, i, entry)),
	)

def _create_entry(req, i, entry):
	entry_tmpl = req.app.jinja_env.get_template('Entry.xml')
	return entry_tmpl.render(entry = entry, i = i)

def _create_category(req, locale, category, i):
	category_tmpl = req.app.jinja_env.get_template('Category.xml')
	extra = None
	if req.method == 'POST':
		extra = Markup(' diffgr:id="Category{}" msdata:rowOrder="{}" diffgr:hasChanges="inserted"'.format(i + 1, i))
	
	return category_tmpl.render(
		extra = extra,
		locale = locale,
		category = category,
	)

def _create_diffgram(req, results):
	diffgram_tmpl = req.app.jinja_env.get_template('diffgram.xml')
	return diffgram_tmpl.render(
		results = results,
	)

async def _preprocess_soap(req):
	from lxml.objectify import fromstring as parse_xml
	
	body = await req.read()
	root = parse_xml(body)
	
	action = _find_element(root, 'Body/*[1]')
	
	return action

def _get_tag_localname(elm):
	return lxml.etree.QName(elm.tag).localname

def _bool_to_str(b):
	return 'True' if b else 'False'

def render(req, tmpl_name, ctxt = None, status = 200):
	if tmpl_name.endswith('.xml'):
		content_type = 'text/xml'
	else:
		content_type = 'text/html'
	tmpl = req.app.jinja_env.get_template(tmpl_name)
	content = tmpl.render(**(ctxt or {}))
	return web.Response(status = status, content_type = content_type, text = content)

def _find_element(xml, query):
	thing = xml.find('.//{*}' + query.replace('/', '/{*}'))
	if isinstance(thing, lxml.objectify.StringElement):
		thing = str(thing)
	elif isinstance(thing, lxml.objectify.BoolElement):
		thing = bool(thing)
	elif isinstance(thing, lxml.objectify.IntElement):
		thing = int(thing)
	return thing

APP_ENTRY_JS = '''		[{}, [{}, {}, {}, ""]],
'''